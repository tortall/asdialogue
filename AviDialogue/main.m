//
//  main.m
//  AviDialogue
//
//  Created by Avi Shevin on 8/27/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
