//
//  ANSTofessAppearance.m
//  AviDialogue
//
//  Created by Avi Shevin on 9/5/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import "ANSTofessAppearance.h"

@implementation ANSTofessAppearance

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.headerFont = [UIFont boldSystemFontOfSize:14];
        self.headerColor = [UIColor darkTextColor];
        self.headerHeight = 30;
        
        self.radioFont = [UIFont systemFontOfSize:14];
        self.radioColor = [UIColor darkTextColor];
        self.radioHeight = 30;
        
        self.switchFont = [UIFont systemFontOfSize:14];
        self.switchColor = [UIColor darkTextColor];
        self.switchHeight = 35;
        
        self.placeholderFont = [UIFont italicSystemFontOfSize:14];
        self.placeholderColor = [UIColor lightGrayColor];
        
        self.textFont = [UIFont systemFontOfSize:14];
        self.textColor = [UIColor darkTextColor];
        self.textHeight = 30;
        
        self.sectionHeaderFont = [UIFont systemFontOfSize:10];
        self.sectionHeaderColor = [UIColor darkGrayColor];
        self.sectionHeaderHeight = 40;
        
        self.navigationTintColor = [[UINavigationBar appearance] tintColor];
        self.navigationTintColor = [[UINavigationBar appearance] barTintColor];
    }
    return self;
}

- (void)setFont:(UIFont *)font {
    _font = self.headerFont = self.radioFont = self.placeholderFont = self.textFont = font;
}

- (void)setColor:(UIColor *)color {
    _color = self.headerColor = self.radioColor = self.placeholderColor = self.textColor = color;
}

- (void)setHeight:(CGFloat)height {
    _height = self.headerHeight = self.radioHeight = self.textHeight = height;
}

@end
