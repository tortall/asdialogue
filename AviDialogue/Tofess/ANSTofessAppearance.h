//
//  ANSTofessAppearance.h
//  AviDialogue
//
//  Created by Avi Shevin on 9/5/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ANSTofessAppearance : NSObject

@property (nonatomic, strong) UIFont *headerFont;
@property (nonatomic, strong) UIColor *headerColor;
@property (nonatomic, assign) CGFloat headerHeight;

@property (nonatomic, strong) UIFont *radioFont;
@property (nonatomic, strong) UIColor *radioColor;
@property (nonatomic, assign) CGFloat radioHeight;

@property (nonatomic, strong) UIFont *switchFont;
@property (nonatomic, strong) UIColor *switchColor;
@property (nonatomic, assign) CGFloat switchHeight;

@property (nonatomic, strong) UIFont *placeholderFont;
@property (nonatomic, strong) UIColor *placeholderColor;

@property (nonatomic, strong) UIFont *textFont;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, assign) CGFloat textHeight;

@property (nonatomic, strong) UIFont *sectionHeaderFont;
@property (nonatomic, strong) UIColor *sectionHeaderColor;
@property (nonatomic, assign) CGFloat sectionHeaderHeight;

@property (nonatomic, strong) UIColor *navigationTintColor;
@property (nonatomic, strong) UIColor *navigationBarTintColor;

@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, assign) CGFloat height;

@end
