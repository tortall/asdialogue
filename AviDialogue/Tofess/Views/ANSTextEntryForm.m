//
//  ANSTextEntryForm.m
//  AviDialogue
//
//  Created by Avi Shevin on 9/5/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import "ANSTextEntryForm.h"

@interface ANSTextEntryForm() <UITextViewDelegate>

@property (nonatomic, strong) UITextView *textView;

@end

@implementation ANSTextEntryForm
{
    NSTimeInterval _duration;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layoutSubviews {
    [UIView animateWithDuration:_duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.textView.frame = self.bounds;
    } completion:nil];
}

- (void) createView {
    self.clipsToBounds = YES;
    
    self.textView = ({
        UITextView *tv = [[UITextView alloc] initWithFrame:self.bounds];
        
        tv.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
        tv.delegate = self;
        
        [self addSubview:tv];
        
        tv;
    });
}

- (BOOL)becomeFirstResponder {
    return [self.textView becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    return [self.textView resignFirstResponder];
}

- (void) keyboardWillShow:(NSNotification *)note {
    UIView *window = [[UIApplication sharedApplication].delegate window];
    
    CGRect myFrame = self.frame;
    
    CGRect kbFrame = [self convertRect:[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue] fromView:window];
    
    myFrame.size.height = MIN(kbFrame.origin.y, myFrame.size.height);
    
    [((NSValue *)note.userInfo[UIKeyboardAnimationDurationUserInfoKey]) getValue:&_duration];
    
    [UIView animateWithDuration:_duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.frame = myFrame;
    } completion:nil];
}

- (void) keyboardWillHide:(NSNotification *)note {
    UIView *window = [[UIApplication sharedApplication].delegate window];
    
    CGRect myFrame = self.frame;
    
    CGRect kbFrame = [self convertRect:[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue] fromView:window];
    
    myFrame.size.height = kbFrame.origin.y;
    
    self.frame = myFrame;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    [self.delegate textEntryFormDidChange:self];
}

#pragma mark - Properties

- (void)setText:(NSString *)text {
    self.textView.text = text;
}

- (NSString *)text {
    return self.textView.text;
}

@end
