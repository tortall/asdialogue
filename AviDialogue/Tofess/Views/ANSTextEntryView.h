//
//  ANSTextEntryView.h
//  AviDialogue
//
//  Created by Avi Shevin on 9/1/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ANSTextEntryView, ANSTofessAppearance;

@protocol ANSTextEntryViewDelegate <NSObject>

- (void) textEntryViewDoneTapped:(ANSTextEntryView *)textEntryView;
- (void) textEntryViewCancelTapped:(ANSTextEntryView *)textEntryView;

@end


@interface ANSTextEntryView : UIView

@property (nonatomic, weak) id<ANSTextEntryViewDelegate> delegate;
@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *text;

@property (nonatomic, weak) ANSTofessAppearance *tofessAppearance;

@end
