//
//  ANSTextEntryForm.h
//  AviDialogue
//
//  Created by Avi Shevin on 9/5/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ANSTextEntryForm;

@protocol ANSTextEntryFormDelegate <NSObject>

- (void) textEntryFormDidChange:(ANSTextEntryForm *)textEntryForm;

@end


@interface ANSTextEntryForm : UIView

@property (nonatomic, weak) id<ANSTextEntryFormDelegate> delegate;

@property (nonatomic, strong) NSString *text;

@end
