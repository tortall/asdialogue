//
//  ANSTextEntryView.m
//  AviDialogue
//
//  Created by Avi Shevin on 9/1/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import "ANSTextEntryView.h"
#import "ANSTofessAppearance.h"

@interface ANSTextEntryView()

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UINavigationBar *navBar;

@end

@implementation ANSTextEntryView
{
    NSTimeInterval _duration;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layoutSubviews {
    CGRect navFrame = self.navBar.frame;
    
    navFrame.size.width = self.bounds.size.width - 20;
    
    [UIView animateWithDuration:_duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.navBar.frame = navFrame;
        
        self.textView.frame = CGRectMake(navFrame.origin.x, navFrame.size.height, navFrame.size.width, self.bounds.size.height - navFrame.size.height - 10);
    } completion:nil];
}

- (void) createView {
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    self.navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(10, 10, self.bounds.size.width - 20, 44)];

    self.navBar.tintColor = self.tofessAppearance.navigationTintColor;
    self.navBar.barTintColor = self.tofessAppearance.navigationBarTintColor;
    
    [self addSubview:self.navBar];
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:self.title];
    item.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    item.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    [self.navBar setItems:@[ item ]];
    
    item.prompt = item.title;
    item.title = nil;
    
    CGRect navFrame = self.navBar.frame;
    
    self.textView = ({
        UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(navFrame.origin.x, navFrame.size.height, navFrame.size.width, self.bounds.size.height - navFrame.size.height - 10)];
        
        tv.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
        
        [self addSubview:tv];
        
        tv;
    });
}

- (void) done:(id)button {
    [self.delegate textEntryViewDoneTapped:self];
}

- (void) cancel:(id)button {
    [self.delegate textEntryViewCancelTapped:self];
}

- (BOOL)becomeFirstResponder {
    return [self.textView becomeFirstResponder];
}

- (void) keyboardWillShow:(NSNotification *)note {
    UIView *window = [[UIApplication sharedApplication].delegate window];
    
    CGRect myFrame = self.frame;
    
    CGRect kbFrame = [self convertRect:[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue] fromView:window];
    
    myFrame.size.height = MIN(kbFrame.origin.y, myFrame.size.height);
    
    [((NSValue *)note.userInfo[UIKeyboardAnimationDurationUserInfoKey]) getValue:&_duration];
    
    [UIView animateWithDuration:_duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.frame = myFrame;
    } completion:nil];
}

- (void) keyboardWillHide:(NSNotification *)note {
    UIView *window = [[UIApplication sharedApplication].delegate window];
    
    CGRect myFrame = self.frame;
    
    CGRect kbFrame = [self convertRect:[note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue] fromView:window];
    
    myFrame.size.height = kbFrame.origin.y;
    
    self.frame = myFrame;
}

#pragma mark - Properties

- (void)setTitle:(NSString *)title {
    _title = title;
    
    [self.navBar.items[0] setPrompt:title];
}

- (void)setText:(NSString *)text {
    self.textView.text = text;
}

- (NSString *)text {
    return self.textView.text;
}

-(void)setTofessAppearance:(ANSTofessAppearance *)tofessAppearance {
    _tofessAppearance = tofessAppearance;
    
    self.navBar.tintColor = self.tofessAppearance.navigationTintColor;
    self.navBar.barTintColor = self.tofessAppearance.navigationBarTintColor;
}

@end
