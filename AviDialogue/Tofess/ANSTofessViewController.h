//
//  ANSTofessViewController.h
//  AviDialogue
//
//  Created by Avi Shevin on 8/31/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANSTofessAppearance.h"

@class ANSTofessElementChange;

typedef void(^ANSTofessUpdateBlock)(ANSTofessElementChange *change);
typedef BOOL(^ANSTofessValidateBlock)(NSDictionary *formDefinition, NSDictionary *formData);
typedef void(^ANSTofessDoneBlock)(NSDictionary *formDefinition, NSDictionary *formData);
typedef void(^ANSTofessBackBlock)();
typedef NSString*(^ANSTofessLocalizationBlock)(NSString *key);


@interface ANSTofessViewController : UIViewController

- (id) initWithFormDefinition:(NSDictionary *)formDefinition;

@property (nonatomic, copy) ANSTofessUpdateBlock updateBlock;
@property (nonatomic, copy) ANSTofessValidateBlock validateBlock;
@property (nonatomic, copy) ANSTofessDoneBlock doneBlock;
@property (nonatomic, copy) ANSTofessBackBlock backBlock;
@property (nonatomic, copy) ANSTofessLocalizationBlock localizationBlock;

@property (nonatomic, strong) ANSTofessAppearance *tofessAppearance;

@end


@interface ANSTofessElement : NSObject

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSArray *switchValues;

@end


@interface ANSTofessElementChange : NSObject

@property (nonatomic, strong) NSString *sectionName;

@property (nonatomic, copy) ANSTofessElement *previous;
@property (nonatomic, copy) ANSTofessElement *current;

@end
