//
//  ANSTofessViewController.m
//  AviDialogue
//
//  Created by Avi Shevin on 8/31/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import "ANSTofessViewController.h"
#import "ANSTextEntryView.h"
#import "ANSTextEntryForm.h"

@interface ANSTofessElement() <NSCopying>

@end

@implementation ANSTofessElement

+ (instancetype) tofessElementWithIndex:(NSInteger)index {
    ANSTofessElement *e = [[ANSTofessElement alloc] init];
    
    e.index = index;

    return e;
}

- (id)copyWithZone:(NSZone *)zone {
    ANSTofessElement *data = [[ANSTofessElement alloc] init];
    data.index = self.index;
    data.text = self.text;
    data.switchValues = self.switchValues;
    
    return data;
}

@end

@implementation ANSTofessElementChange

- (id) init {
    self = [super init];
    if ( self ) {
        self.previous = [[ANSTofessElement alloc] init];
        self.current = [[ANSTofessElement alloc] init];
    }
    
    return self;
}

@end


@interface ANSSwitchKey : NSObject <NSCopying>

@property (nonatomic, strong) NSDictionary *sectionDefinition;
@property (nonatomic, assign) NSInteger index;

@end

@implementation ANSSwitchKey

- (id)copyWithZone:(NSZone *)zone {
    ANSSwitchKey *sk = [[ANSSwitchKey alloc] init];
    sk.sectionDefinition = self.sectionDefinition;
    sk.index = self.index;
    
    return sk;
}

@end


@interface ANSTofessViewController () <UITableViewDelegate, UITableViewDataSource, ANSTextEntryViewDelegate, ANSTextEntryFormDelegate>

@property (nonatomic, copy) NSDictionary *formDefinition;

@property (nonatomic, strong) NSMutableArray *viewStack;
@property (nonatomic, strong) NSMutableArray *formStack;

@property (nonatomic, strong) NSMutableDictionary *formData;
@property (nonatomic, assign) NSDictionary *currentForm;
@property (nonatomic, strong) NSMutableDictionary *dependencies;

@property (nonatomic, strong) NSArray *visibleSections;

@property (nonatomic, strong) UINavigationBar *navBar;
@property (nonatomic, strong) UIView *formView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ANSTextEntryForm *textEntryFormView;

@property (nonatomic, strong) ANSTextEntryView *textEntryView;

@property (nonatomic, strong) NSDictionary *textSection;
@property (nonatomic, strong) NSIndexPath *textIndexPath;

@property (nonatomic, strong) NSMutableDictionary *switches;

@property (nonatomic, assign) BOOL navigationBarWasHidden;

@end

@implementation ANSTofessViewController

- (id) initWithFormDefinition:(NSDictionary *)formDefinition {
    self = [super init];
    if ( self ) {
        self.formDefinition = formDefinition;
        self.currentForm = formDefinition;
        
        self.viewStack = [NSMutableArray array];
        self.formStack = [NSMutableArray array];
        
        self.formData = [NSMutableDictionary dictionary];
        
        self.dependencies = [NSMutableDictionary dictionary];
        
        self.switches = [NSMutableDictionary dictionary];
        
        [self parseFormDefinition:self.currentForm];
        
        self.tofessAppearance = [[ANSTofessAppearance alloc] init];
    }
    
    return self;
}

- (void)loadView {
    UIView *view = [[UIView alloc] initWithFrame:[[UIApplication sharedApplication].keyWindow bounds]];
    
    view.backgroundColor = [UIColor whiteColor];

    self.navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, view.bounds.size.width, 44)];
    
    [view addSubview:self.navBar];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navBar.frame), view.bounds.size.width, view.bounds.size.height - CGRectGetMaxY(self.navBar.frame))
                                                  style:UITableViewStyleGrouped];
    self.formView = self.tableView;
    
    [view addSubview:self.tableView];
    
    [self configureView];

    self.view = view;
    
    self.navigationBarWasHidden = self.navigationController.navigationBarHidden;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = self.navigationBarWasHidden;
}

- (void) configureView {
    [self configureNavBar];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView reloadData];
}

- (void) configureNavBar {
    self.navBar.tintColor = self.tofessAppearance.navigationTintColor;
    self.navBar.barTintColor = self.tofessAppearance.navigationBarTintColor;
    
    NSString *backTitle = [@"❮ " stringByAppendingString:[self localizedString:self.currentForm[@"form"][@"back"]]];
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:[self localizedString:self.currentForm[@"form"][@"title"]]];
    
    item.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:backTitle style:UIBarButtonItemStylePlain target:nil action:nil];
    item.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[self localizedString:self.currentForm[@"form"][@"done"]] style:UIBarButtonItemStyleDone target:nil action:nil];
    
    [item.leftBarButtonItem setAction:@selector(backAction:)];
    [item.rightBarButtonItem setAction:@selector(doneAction:)];
    
    if ( self.validateBlock ) {
        item.rightBarButtonItem.enabled = self.validateBlock(self.currentForm, self.formData);
    }
    
    [self.navBar setItems:@[ item ]];
}

- (void) backAction:(id)button {
    if ( self.formStack.count > 0 && self.viewStack.count > 0 ) {
        self.currentForm = self.formStack.lastObject;
        [self.formStack removeLastObject];
        
        [self configureNavBar];
        
        [self transitionToOldSection];
    } else {
        if ( self.backBlock != nil ) {
            self.backBlock();
        }
    }
}

- (void) doneAction:(id)button {
    if ( self.doneBlock != nil ) {
        self.doneBlock(self.currentForm, self.formData);
    }
}

#pragma mark -

- (void) parseFormDefinition:(NSDictionary *)form {
    [form[@"sections"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *section = obj;
        
        if ( section[@"dependency"] != nil ) {
            NSString *sectionDep = section[@"dependency"][@"section"];
            NSString *itemDep = section[@"dependency"][@"item"];
            
            self.dependencies[sectionDep] = itemDep;
        }
        
        if ( [section[@"type"] isEqualToString:@"radio"] ) {
            self.formData[section[@"name"]] = [ANSTofessElement tofessElementWithIndex:-1];
        }
        
        if ( [section[@"type"] isEqualToString:@"switch"] ) {
            NSMutableArray *values = [NSMutableArray array];
            for ( int i = 0; i < [section[@"items"] count]; i++ ) {
                [values addObject:@(0)];
            }
            
            ANSTofessElement *data = [[ANSTofessElement alloc] init];
            data.switchValues = values;
            
            self.formData[section[@"name"]] = data;
        }

        if ( [section[@"type"] isEqualToString:@"header"] ) {
            [section[@"subforms"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [self parseFormDefinition:obj];
            }];
        }
    }];
}

- (NSString *) localizedString:(NSString *)string {
    return ( string != nil ) ? ( self.localizationBlock != nil ) ? self.localizationBlock(string) : NSLocalizedString(string, nil) : @"";
}

- (UIView *) viewForForm:(NSDictionary *)form {
    if ( [form[@"sections"] count] == 1 && [form[@"sections"][0][@"type"] isEqualToString:@"text"] ) {
        ANSTextEntryForm *textEntryForm = [[ANSTextEntryForm alloc] init];
        
        textEntryForm.delegate = self;
        
        return textEntryForm;
    } else {
        UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        
        tableView.delegate = self;
        tableView.dataSource = self;
        
        return tableView;
    }
    
    return nil;
}

- (void) transitionToNewForm:(NSDictionary *)form {
    [self.formStack addObject:self.currentForm];
    self.currentForm = form;
    
    [self configureNavBar];
    
    [self.viewStack addObject:self.formView];
    
    UIView *currentView = self.formView;
    UIView *newView = [self viewForForm:form];
    
    CGRect currentRect = currentView.frame;
    currentRect.origin.x -= 40;
    
    CGRect newFrame = self.formView.frame;
    newFrame.origin.x = self.view.bounds.size.width;
    
    newView.frame = newFrame;
    
    newFrame.origin.x = 0;
    
    [self.view addSubview:newView];
    
    if ( [newView isKindOfClass:[UITableView class]] ) {
        [(UITableView *)newView reloadData];
    }
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        currentView.frame = currentRect;
        newView.frame = newFrame;
        
    } completion:^(BOOL finished) {
        currentView.frame = newFrame;
        
        self.formView = newView;
        
        if ( [newView isKindOfClass:[UITableView class]] ) {
            self.tableView = (UITableView *)newView;
        }
        
        if ( [newView isKindOfClass:[ANSTextEntryForm class]] ) {
            self.textEntryFormView = (ANSTextEntryForm *)newView;
            
            self.textSection = self.currentForm[@"sections"][0];

            ANSTofessElement *current = self.formData[self.textSection[@"name"]];
            
            self.textEntryFormView.text = current.text;
            
            [self.textEntryFormView becomeFirstResponder];
        }
    }];
}

- (void) transitionToOldSection {
    UIView *currentView = self.formView;
    UIView *newView = [self.viewStack lastObject];

    [self.viewStack removeLastObject];

    CGRect currentRect = currentView.frame;
    currentRect.origin.x = self.view.bounds.size.width;
    
    CGRect newFrame = newView.frame;
    newFrame.origin.x = -40;
    
    newView.frame = newFrame;
    
    newFrame.origin.x = 0;
    
    if ( [newView isKindOfClass:[UITableView class]] ) {
        [(UITableView *)newView reloadData];
    }
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        currentView.frame = currentRect;
        newView.frame = newFrame;
        
    } completion:^(BOOL finished) {
        self.formView = newView;
        
        [currentView removeFromSuperview];
        
        if ( self.validateBlock != nil ) {
            self.navBar.topItem.rightBarButtonItem.enabled = self.validateBlock(self.currentForm, self.formData);
        }
        
        self.tableView = nil;
        self.textEntryFormView = nil;

        if ( [newView isKindOfClass:[UITableView class]] ) {
            self.tableView = (UITableView *)newView;
        }
        
        if ( [newView isKindOfClass:[ANSTextEntryForm class]] ) {
            self.textEntryFormView = (ANSTextEntryForm *)newView;
        }
    }];
}

#pragma mark -

- (BOOL)resignFirstResponder {
    [self.textEntryFormView resignFirstResponder];
    [self.textEntryView resignFirstResponder];
    
    return YES;
}

#pragma mark -

- (NSArray *) visibleSections {
    NSMutableArray *visibleSections = [NSMutableArray array];
    
    for ( NSDictionary *sectionDef in self.currentForm[@"sections"] ) {
        if ( sectionDef[@"dependency"] != nil ) {
            NSString *section = sectionDef[@"dependency"][@"section"];
            
            NSDictionary *depSection = [[self.currentForm[@"sections"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name == %@", section]] lastObject];
            
            if ( [depSection[@"type"] isEqualToString:@"radio"] ) {
                NSString *item = sectionDef[@"dependency"][@"item"];
                
                ANSTofessElement *data = self.formData[section];
                
                if ( [data.text isEqualToString:item] ) {
                    [visibleSections addObject:sectionDef];
                }
            }
            
            if ( [depSection[@"type"] isEqualToString:@"switch"] ) {
                ANSTofessElement *data = self.formData[section];
                
                NSString *item = sectionDef[@"dependency"][@"item"];
                BOOL position = [sectionDef[@"dependency"][@"position"] boolValue];
                
                if ( [data.switchValues[[((NSArray*)depSection[@"items"]) indexOfObject:item]] boolValue] == position ) {
                    [visibleSections addObject:sectionDef];
                }
            }
        } else {
            [visibleSections addObject:sectionDef];
        }
    }
    
    return [visibleSections copy];
}

- (void) switchChange:(UISwitch *)switchView {
    [self.switches enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        ANSSwitchKey *sk = key;
        
        if ( switchView == obj ) {
            ANSTofessElement *data = self.formData[sk.sectionDefinition[@"name"]];
            
            ANSTofessElementChange *change = [[ANSTofessElementChange alloc] init];
            change.sectionName = sk.sectionDefinition[@"name"];
            change.previous = data;
            
            NSMutableArray *a = [NSMutableArray arrayWithArray:data.switchValues];
            a[sk.index] = @(switchView.on);
            data.switchValues = [a copy];
            
            change.current = data;
            
            if ( self.updateBlock != nil ) {
                self.updateBlock(change);
            }
            
            self.formData[sk.sectionDefinition[@"name"]] = data;
            
            if ( self.validateBlock != nil ) {
                self.navBar.topItem.rightBarButtonItem.enabled = self.validateBlock(self.currentForm, self.formData);
            }
            
            NSString *depItem = self.dependencies[sk.sectionDefinition[@"name"]];
            if ( depItem != nil ) {
                [self.tableView reloadData];
            }
            
            *stop = YES;
        }
    }];
}

#pragma mark - Table View

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return ( [self.visibleSections[section][@"title"] length] > 0 ) ? self.tofessAppearance.sectionHeaderHeight : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *formSection = self.visibleSections[indexPath.section];
    
    if ( [formSection[@"type"] isEqualToString:@"radio"] ) {
        return self.tofessAppearance.radioHeight;
    }
    
    if ( [formSection[@"type"] isEqualToString:@"switch"] ) {
        return self.tofessAppearance.switchHeight;
    }
    
    if ( [formSection[@"type"] isEqualToString:@"header"] ) {
        return self.tofessAppearance.headerHeight;
    }
    
    if ( [formSection[@"type"] isEqualToString:@"text"] ) {
        return self.tofessAppearance.textHeight;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.visibleSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *formSection = self.visibleSections[section];
    
    if ( [formSection[@"type"] isEqualToString:@"radio"] ) {
        return [formSection[@"items"] count];
    }
    
    if ( [formSection[@"type"] isEqualToString:@"switch"] ) {
        return [formSection[@"items"] count];
    }
    
    if ( [formSection[@"type"] isEqualToString:@"header"] ) {
        return [formSection[@"items"] count];
    }
    
    if ( [formSection[@"type"] isEqualToString:@"text"] ) {
        return 1;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *formSection = self.visibleSections[indexPath.section];
    
    if ( [formSection[@"type"] isEqualToString:@"radio"] ) {
        return [self cellForRadioItemInFormSection:formSection atIndexPath:indexPath];
    }
    
    if ( [formSection[@"type"] isEqualToString:@"switch"] ) {
        return [self cellForSwitchItemInFormSection:formSection atIndexPath:indexPath];
    }
    
    if ( [formSection[@"type"] isEqualToString:@"header"] ) {
        return [self cellForHeaderItemInFormSection:formSection atIndexPath:indexPath];
    }
    
    if ( [formSection[@"type"] isEqualToString:@"text"] ) {
        return [self cellForTextItemInFormSection:formSection atIndexPath:indexPath];
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, self.tofessAppearance.sectionHeaderHeight)];
    view.backgroundColor = [UIColor clearColor];
    
    NSString *text = [self localizedString:self.visibleSections[section][@"title"]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width - 30, self.tofessAppearance.sectionHeaderHeight)];
    
    label.font = self.tofessAppearance.sectionHeaderFont;
    label.textColor = self.tofessAppearance.sectionHeaderColor;
    label.text = text;
    
    [label sizeToFit];
    
    CGRect frame = label.frame;
    frame.origin.y = view.bounds.size.height - frame.size.height - 2;
    label.frame = frame;
    
    [view addSubview:label];
    
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSDictionary *section = self.visibleSections[indexPath.section];
    
    if ( [section[@"type"] isEqualToString:@"radio"] ) {
        [self selectedRadioItemInFormSection:section atIndexPath:indexPath];
    }
    
    if ( [section[@"type"] isEqualToString:@"switch"] ) {
        UISwitch *sv = (UISwitch *)[tableView cellForRowAtIndexPath:indexPath].accessoryView;
        sv.on = ! sv.on;
        [self switchChange:sv];
    }
    
    if ( [section[@"type"] isEqualToString:@"header"] ) {
        [self transitionToNewForm:section[@"subforms"][indexPath.row]];
    }
    
    if ( [section[@"type"] isEqualToString:@"text"] ) {
        [self selectedTextItemInFormSection:section atIndexPath:indexPath];
    }
}

#pragma mark - Radio

- (UITableViewCell *)cellForRadioItemInFormSection:(NSDictionary *)formSection atIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"radio"];
    
    cell.textLabel.text = [self localizedString:formSection[@"items"][indexPath.row]];
    cell.textLabel.font = self.tofessAppearance.radioFont;
    
    ANSTofessElement *data = self.formData[formSection[@"name"]];
    if ( data != nil ) {
        cell.accessoryType = ( data.index == indexPath.row ) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void) selectedRadioItemInFormSection:(NSDictionary *)formSection atIndexPath:(NSIndexPath *)indexPath {
    ANSTofessElementChange *change = [[ANSTofessElementChange alloc] init];
    
    ANSTofessElement *rs = self.formData[formSection[@"name"]];
    
    if ( rs.index >= 0 ) {
        [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rs.index inSection:indexPath.section]].accessoryType = UITableViewCellAccessoryNone;
    }
    
    [self.tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    
    if ( rs.index != indexPath.row ) {
        change.sectionName = formSection[@"name"];
        
        change.previous.index = rs.index;
        change.previous.text = rs.text;
        
        rs.index = indexPath.row;
        rs.text = formSection[@"items"][indexPath.row];
        
        change.current.index = rs.index;
        change.current.text = rs.text;
        
        if ( self.updateBlock ) {
            self.updateBlock(change);
        }
        
        self.formData[formSection[@"name"]] = change.current;
        
        if ( self.validateBlock != nil ) {
            self.navBar.topItem.rightBarButtonItem.enabled = self.validateBlock(self.currentForm, self.formData);
        }
        
        NSString *depItem = self.dependencies[formSection[@"name"]];
        if ( depItem != nil && ( [change.current.text isEqualToString:depItem] || [change.previous.text isEqualToString:depItem] ) ) {
            [self.tableView reloadData];
        }
    }
}

#pragma mark - Switch

- (UITableViewCell *)cellForSwitchItemInFormSection:(NSDictionary *)formSection atIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"switch"];
    
    cell.textLabel.text = [self localizedString:formSection[@"items"][indexPath.row]];
    cell.textLabel.font = self.tofessAppearance.radioFont;
    
    UISwitch *switchView = [[UISwitch alloc] init];
    
    [switchView addTarget:self action:@selector(switchChange:) forControlEvents:UIControlEventValueChanged];
    
    ANSSwitchKey *sk = [[ANSSwitchKey alloc] init];
    sk.sectionDefinition = formSection;
    sk.index = indexPath.row;
    
    self.switches[sk] = switchView;
    
    ANSTofessElement *data = self.formData[formSection[@"name"]];
    if ( data != nil ) {
        switchView.on = [data.switchValues[indexPath.row] boolValue];
    }
    
    cell.accessoryView = switchView;
    
    return cell;
}

#pragma mark - Header

- (UITableViewCell *)cellForHeaderItemInFormSection:(NSDictionary *)formSection atIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"header"];
    
    cell.textLabel.text = [self localizedString:formSection[@"items"][indexPath.row]];
    cell.textLabel.font = self.tofessAppearance.headerFont;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - Text

- (UITableViewCell *)cellForTextItemInFormSection:(NSDictionary *)formSection atIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"text"];
    
    NSString *text = [self.formData[formSection[@"name"]] text];
    
    cell.textLabel.text = ( text.length > 0 ) ? text : [self localizedString:formSection[@"placeholder"]];
    cell.textLabel.textColor = ( text.length > 0 ) ? [UIColor darkTextColor] : [UIColor lightGrayColor];
    cell.textLabel.font = ( text.length > 0 ) ? self.tofessAppearance.textFont : self.tofessAppearance.placeholderFont;
    
    return cell;
}

- (void) selectedTextItemInFormSection:(NSDictionary *)formSection atIndexPath:(NSIndexPath *)indexPath {
    CGRect cellFrame = [self.view convertRect:[self.tableView cellForRowAtIndexPath:indexPath].frame fromView:self.tableView];
    
    CGRect frame = self.view.bounds;
    
    frame.origin.y = self.navBar.frame.origin.y;
    frame.size.height -= self.navBar.frame.origin.y;
    
    self.textEntryView = [[ANSTextEntryView alloc] initWithFrame:cellFrame];
    
    self.textEntryView.tofessAppearance = self.tofessAppearance;
    self.textEntryView.delegate = self;
    self.textEntryView.text = [self.formData[formSection[@"name"]] text];

    NSString *title = [self localizedString:formSection[@"title"]];
    if ( title.length == 0 ) {
        title = [self localizedString:formSection[@"prompt"]];
    }
    self.textEntryView.title = title;

    [self.view addSubview:self.textEntryView];
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.textEntryView.frame = frame;
    } completion:^(BOOL finished) {
        [self.textEntryView becomeFirstResponder];
    }];
    
    self.textSection = formSection;
    self.textIndexPath = indexPath;
}

#pragma mark - Properties

- (void)setValidateBlock:(ANSTofessValidateBlock)validateBlock {
    _validateBlock = validateBlock;
    
    if ( validateBlock ) {
        self.navBar.topItem.rightBarButtonItem.enabled = validateBlock(self.currentForm, [self.formData copy]);
    }
}

#pragma mark - ANSTextEntryViewDelegate

- (void)textEntryViewDoneTapped:(ANSTextEntryView *)textEntryView {
    ANSTofessElement *current = self.formData[self.textSection[@"name"]];
    
    ANSTofessElementChange *change = [[ANSTofessElementChange alloc] init];
    
    change.sectionName = self.textSection[@"name"];
    change.previous.text = current.text;
    change.current.text = textEntryView.text;

    if ( self.updateBlock != nil ) {
        self.updateBlock(change);
    }
    
    self.formData[self.textSection[@"name"]] = change.current;
    
    if ( self.validateBlock != nil ) {
        self.navBar.topItem.rightBarButtonItem.enabled = self.validateBlock(self.currentForm, self.formData);
    }
    
    [self.tableView reloadRowsAtIndexPaths:@[ self.textIndexPath ] withRowAnimation:UITableViewRowAnimationNone];
    [textEntryView removeFromSuperview];
    
    self.textEntryView = nil;
}

- (void)textEntryViewCancelTapped:(ANSTextEntryView *)textEntryView {
    [textEntryView removeFromSuperview];
    
    self.textEntryView = nil;
}

#pragma mark - ANSTextEntryFormDelegate

- (void)textEntryFormDidChange:(ANSTextEntryForm *)textEntryForm {
    ANSTofessElement *current = self.formData[self.textSection[@"name"]];
    
    ANSTofessElementChange *change = [[ANSTofessElementChange alloc] init];
    
    change.sectionName = self.textSection[@"name"];
    change.previous.text = current.text;
    change.current.text = textEntryForm.text;
    
    if ( self.updateBlock != nil ) {
        self.updateBlock(change);
    }
    
    self.formData[self.textSection[@"name"]] = change.current;
    
    if ( self.validateBlock != nil ) {
        self.navBar.topItem.rightBarButtonItem.enabled = self.validateBlock(self.currentForm, self.formData);
    }
}

@end
