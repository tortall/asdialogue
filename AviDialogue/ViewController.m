//
//  ViewController.m
//  AviDialogue
//
//  Created by Avi Shevin on 8/27/14.
//  Copyright (c) 2014 Avi Shevin. All rights reserved.
//

#import "ViewController.h"
#import "ANSTofessViewController.h"

@interface ViewController ()

@property (nonatomic, weak) UIView *testView;
@property (nonatomic, weak) UILabel *testLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"טופס";
}

- (IBAction)formTapped:(id)button {
    NSDictionary *formDef =
    @{
      @"form" : @{
              @"name" : @"main",
              @"back" : @"More",
              @"title" : @"Contact Us",
              },
      @"sections" : @[
              @{
                  @"name" : @"categories",
                  @"title" : @"HOW CAN WE HELP YOU?",
                  @"type" : @"header",
                  @"items" : @[
                          @"Feature Request",
                          @"Bugs/Problem",
                          @"General Feedback",
                          ],
                  @"subforms" : @[
                          @{
                              @"form" : @{
                                      @"name" : @"request",
                                      @"back" : @"Contact Us",
                                      @"title" : @"Feature Request",
                                      @"done" : @"Send"
                                      },
                              @"sections" : @[
                                      @{
                                          @"name" : @"feature",
                                          @"placeholder" : @"Description",
                                          @"type" : @"text",
                                          },
                                      ]
                              },
                          @{
                              @"form" : @{
                                      @"name" : @"problems",
                                      @"back" : @"Contact Us",
                                      @"title" : @"Bugs/Problem",
                                      @"done" : @"Send"
                                      },
                              @"sections" : @[
                                      @{
                                          @"name" : @"problem",
                                          @"title" : @"WHAT'S WRONG? *",
                                          @"type" : @"radio",
                                          @"items" : @[
                                                  @"Can't send videos",
                                                  @"Can't watch videos",
                                                  @"App is freezing/crashing",
                                                  @"Not getting notifications",
                                                  @"Other",
                                                  ]
                                          },
                                      @{
                                          @"name" : @"miscellaneous",
                                          @"prompt" : @"Describe optional random stuff",
                                          @"placeholder" : @"Optional random stuff",
                                          @"type" : @"text",
                                          @"dependency" : @{ @"section" : @"problem", @"item" : @"Other" }
                                          },
                                      @{
                                          @"name" : @"frequency",
                                          @"title" : @"HOW OFTEN DOES THIS HAPPEN? *",
                                          @"type" : @"radio",
                                          @"items" : @[
                                                  @"Always",
                                                  @"Often",
                                                  @"Just once",
                                                  ]
                                          },
                                      @{
                                          @"name" : @"description",
                                          @"title" : @"PLEASE DESCRIBE THE PROBLEM *",
                                          @"placeholder" : @"Description",
                                          @"type" : @"text",
                                          @"dependency" : @{ @"section" : @"optional", @"item" : @"Disconnected", @"position" : @(YES) }
                                          },
                                      @{
                                          @"name" : @"optional",
                                          @"title" : @"Yo *",
                                          @"type" : @"switch",
                                          @"items" : @[
                                                  @"Miscellaneous",
                                                  @"Disconnected",
                                                  ]
                                          },
                                      ]
                              },
                          @{
                              @"form" : @{
                                      @"name" : @"general",
                                      @"back" : @"Contact Us",
                                      @"title" : @"General Feedback",
                                      @"done" : @"Send"
                                      },
                              @"sections" : @[
                                      @{
                                          @"name" : @"feedback",
                                          @"placeholder" : @"Feedback",
                                          @"type" : @"text",
                                          },
                                      ]
                          },
                          ]
                  },
              ]
      };
    
    ANSTofessViewController *vc = [[ANSTofessViewController alloc] initWithFormDefinition:formDef];
    
    vc.updateBlock = ^(ANSTofessElementChange *change){
        NSLog(@"Section: %@\tOld values: %ld, %@, %@", change.sectionName, (long)change.previous.index, change.previous.text, change.previous.switchValues);
        NSLog(@"Section: %@\tNew values: %ld, %@, %@", change.sectionName, (long)change.current.index, change.current.text, change.current.switchValues);
    };
    
    vc.validateBlock = ^BOOL(NSDictionary *formDefinition, NSDictionary *formData) {
        if ( [formDefinition[@"form"][@"name"] isEqualToString:@"problems"] ) {
            NSString *problem = [formData[@"problem"] text];
            NSString *frequency = [formData[@"frequency"] text];
            NSString *description = [formData[@"description"] text];
            
            return ( problem != nil && frequency != nil && description.length > 2 );
        }
        
        if ( [formDefinition[@"form"][@"name"] isEqualToString:@"request"] ) {
            NSString *feature = [formData[@"feature"] text];
            
            return ( feature.length > 2 );
        }
        
        if ( [formDefinition[@"form"][@"name"] isEqualToString:@"general"] ) {
            NSString *feedback = [formData[@"feedback"] text];
            
            return ( feedback.length > 2 );
        }
        
        return NO;
    };
    
    vc.localizationBlock = ^NSString*(NSString *string) {
        if ( [string isEqualToString:@"Often"] ) {
            return @"🐮";
        }
        
        return string;
    };
    
    vc.doneBlock = ^(NSDictionary *formDefinition, NSDictionary *formData) {
        if ( [formDefinition[@"form"][@"name"] isEqualToString:@"problems"] ) {
            NSString *problem = [formData[@"problem"] text];
            NSString *frequency = [formData[@"frequency"] text];
            NSString *description = [formData[@"description"] text];

            NSLog(@"You submitted a problem report:\nproblem: %@\nfrequency: %@\ndescription %@", problem, frequency, description);
        }
        
        if ( [formDefinition[@"form"][@"name"] isEqualToString:@"request"] ) {
            NSString *feature = [formData[@"feature"] text];
            
            NSLog(@"You submitted a feature request: %@", feature);
        }
        
        if ( [formDefinition[@"form"][@"name"] isEqualToString:@"general"] ) {
            NSString *feedback = [formData[@"feedback"] text];
            
            NSLog(@"You submitted general feedback: %@", feedback);
            
            [vc resignFirstResponder];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Feedback" message:feedback delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            [alert show];
        }
    };
    
    vc.backBlock = ^{
        [self.navigationController popViewControllerAnimated:YES];
    };

    vc.tofessAppearance.navigationBarTintColor = [UIColor colorWithRed:0.0f/255.0f green:165.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
    vc.tofessAppearance.navigationTintColor = [UIColor whiteColor];
    vc.tofessAppearance.sectionHeaderHeight = 25;
    
    id app = [UINavigationBar appearanceWhenContainedIn:[ANSTofessViewController class], nil];
    [app setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
